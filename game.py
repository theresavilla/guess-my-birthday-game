from random import randint

#asking player name
name = input("Hi! What's your name? ")
print("Hi", name)

#setting number of guesses
for guess_num in range(5):
    month = randint(1, 12)
    year = randint(1924, 2004)
    print("Guess " + str(guess_num+1) + ": were you born in " + str(month) + "/"+ str(year) + "?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no" and guess_num < 4:
        print("Drat! Lemme try again!")
    elif answer == "no" and guess_num == 4:
        print("I have other things to do!")
    else:
        print("yes or no only!")
